ARG phpv
FROM php:$phpv-fpm
RUN apt-get update
### For PHP 7.3 libzip-dev needs to be added, builds 7.3 and 7.2 but fails on 7.1 so until more testing we build for 7.2 and 7.1
### as PHP 7.3 is not supported of any kind by Magento
RUN apt-get install -y autoconf git zlib1g-dev libpng-dev libxml2-dev libicu-dev g++ libxslt1-dev libjpeg-dev libfreetype6-dev
RUN if [ "7.2" \> "$PHP_VERSION" ]; then apt-get install libmcrypt-dev -y; fi
RUN docker-php-ext-configure gd --with-jpeg-dir=DIR --with-freetype-dir=/usr/lib64/ \
 && docker-php-ext-configure intl \
 && docker-php-ext-install pdo pdo_mysql zip gd bcmath intl soap xsl

RUN if [ "7.2" \> "$PHP_VERSION" ]; then docker-php-ext-install mcrypt; fi
RUN curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

COPY "php/memory-limit-php.ini" "/usr/local/etc/php/conf.d/memory-limit-php.ini"
COPY "auth/auth.json" "/root/.composer/auth.json"
COPY "shell/start.sh" "/home/root/start.sh"

## RUN cd /var/www/html
## RUN pwd
## RUN git clone https://github.com/magento/magento2.git
## WORKDIR /var/www/html
## RUN rm -rf *

## Steps
# RUN git clone https://github.com/magento/magento2.git .
### RUN composer create-project --repository=https://repo.magento.com/ magento/project-community-edition .
# RUN composer install
# RUN php bin/magento setup:install \
# --base-url=http://localhost/ \
# --db-host=run-db \
# --db-name=test_db \
# --db-user=root \
# --db-password=test_db_pass \
# --backend-frontname=admin \
# --admin-firstname=admin \
# --admin-lastname=admin \
# --admin-email=cristian.vatca@osf-global.com \
# --admin-user=admin \
# --admin-password=Admin1234 \
# --language=en_US \
# --currency=USD \
# --timezone=America/Chicago \
# --use-rewrites=1

# RUN php bin/magento setup:upgrade
# RUN php bin/magento setup:static-content:deploy -f
# RUN chmod -R 755 /var/www/html