Deployment and tests for Magento 2[![Build Status](https://travis-ci.org/)](https://travis-ci.org/)
================
#Overview
This document will help you setup so you can use docker to do tests on different versions of php and magento 2
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

#Requirements
* docker
* docker-compose

#Installation
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Clone or download the repository.

#Getting Started
Modify the .env file to use the versions that you require.
Run the following commands in the folder
```
docker-compose build
docker-compose up
```

#Some info
Database host is the run-db

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact