#!/bin/bash
#testing
cd /var/www/html/
rm -rf nginx.conf.sample
if [ 0 = "$MAGENTO_EDITION" ]; \
then \
    git clone "https://github.com/magento/magento2.git" . \
    # rm -rf *
    git clean -fd \
    git checkout $MAGENTO_VERSION \
    composer install \
; else \
    composer create-project --repository-url=https://repo.magento.com/ magento/project-enterprise-edition:$MAGENTO_VERSION . \
; fi 

php bin/magento setup:install \
 --base-url=http://$BASE_URL/ \
 --db-host=$DB_HOST \
 --db-name=$DB_NAME \
 --db-user=$DB_USER \
 --db-password=$DB_PASSWORD \
 --backend-frontname=$BACKEND_FRONTNAME \
 --admin-firstname=$ADMIN_FIRSTNAME \
 --admin-lastname=$ADMIN_LASTNAME \
 --admin-email=$ADMIN_EMAIL \
 --admin-user=$ADMIN_USER \
 --admin-password=$ADMIN_PASSWORD \
 --language=$MAGENTO_LANGUAGE \
 --currency=$MAGENTO_CURRENCY \
 --timezone=$MAGENTO_TIMEZONE \
 --use-rewrites=1
php bin/magento sampledata:deploy
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy -f
chmod -R 777 /var/www/html

composer config repositories.signifydmage2 git https://github.com/Signifyd/magento2.git
composer require signifyd/module-connect:$SIGNIFYD_VERSION
php bin/magento module:enable Signifyd_Connect
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy -f
chmod -R 777 /var/www/html


#git clone <url> --branch <branch> --single-branch [<folder>]
#git clone --single-branch
#git clone --single-branch --branch <branchname> host:/dir.git
# git clone --single-branch --branch $MAGENTO_VERSION "https://github.com/magento/magento2.git" .

# git clone "https://github.com/magento/magento2.git" .
# git checkout tags/$MAGENTO_VERSION